package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum mi eu magna fermentum, vel luctus tellus semper. Nunc dignissim eleifend ipsum, nec viverra mauris pellentesque non. Fusce auctor ex id mauris egestas, quis luctus nunc pharetra. Sed in dignissim nisi. Aliquam sed tempor urna, nec aliquam mi. Aenean eu feugiat lacus, vel dictum eros. Nulla condimentum porttitor aliquet. Vestibulum vehicula elit non arcu auctor maximus. Quisque est eros, maximus nec diam faucibus, mollis odio."
	fmt.Print("a sebanyak: ") 
	fmt.Println(strings.Count(str, "a"))
	fmt.Print("b sebanyak: ") 
	fmt.Println(strings.Count(str, "b"))
	fmt.Print("i sebanyak: ") 
	fmt.Println(strings.Count(str, "i"))
	fmt.Print("u sebanyak: ") 
	fmt.Println(strings.Count(str, "u"))
	fmt.Print("e sebanyak: ") 
	fmt.Println(strings.Count(str, "e"))
}